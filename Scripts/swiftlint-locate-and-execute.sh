#!/bin/sh

# Parameters:
# 1 - Derived Data location (to resolve SPM location)
# 2 - Optional (--fix) self corrects the source based on ruleset

# Following block is required for arm64 mac devices (different homebrew path than Intel counterpart)
if test -d "/opt/homebrew/bin/"; then
  PATH="/opt/homebrew/bin/:${PATH}"
fi

export PATH

# Attempts to locate the resolved swiftlint repo SPM 
# this is usually the checkout location on local machine, or when derived data is default
spmCheckoutPath=$1/../../../../../../SourcePackages/checkouts

if [ -d $spmCheckoutPath ] 
then
    echo "✅ Directory $spmCheckoutPath exists - resolving path with this location" 
else
    echo "❗ Directory $spmCheckoutPath does not exist - resolving path with alternative location" 
    # when defining parameter output or custom derived data, depth of folder is decreased by 3 levels
    # (usually happens when running on CI)
    spmCheckoutPath=$1/../../../../../../../../../SourcePackages/checkouts

    if [ -d $spmCheckoutPath ] 
    then
        echo "✅ Directory $spmCheckoutPath exists - resolving path with this location" 
    else
        echo "❌ Directory $spmCheckoutPath does not exist - resolving path with alternative location" 
        exit 1
    fi
fi

lintRulePath=$spmCheckoutPath/public-common-ios-swiftlint/Sources/Common-Swiftlint-Package/Resources/mainrules.yml
echo "Lint file: $lintRulePath"

# assuming script will be directly invoked by xcode build - adjust the common lint repository path
if which swiftlint >/dev/null; then
  # check if fix parameter is present
  if [ $2 = "--fix" ]; then
    # calls swiftlint with the autofix option
    swiftlint autocorrect --config $lintRulePath
  fi

  swiftlint --config $lintRulePath
else
  echo "❌ warning: SwiftLint not installed - run bash Scripts/setup-environment.sh"
fi