# Common SwiftLint #

## Installation ##

See [Installation](https://github.com/realm/SwiftLint#installation) in SwiftLint's GitHub project.

## Configuration ##

### Using parent_config ###

You can reference the SwiftLint file remotely by adding the [parent_config](https://github.com/realm/SwiftLint#child--parent-configs-remote) key to your configuration file.
Copy the URL below and replace `<VERSION>` with the version you would like to use in your project (e.g. __0.1.5__):

```
https://bitbucket.org/iversoft/public-common-ios-swiftlint/raw/<VERSION>/Sources/Common-Swiftlint-Package/Resources/mainrules.yml
```

To always use the latest version, replace `<VERSION>` with __master__.

### Using SPM ###

1. From Xcode's File Menu, choose "Add Packages..."

    ![Add package](img/01-addPackage.png "Add package")

2. Add the package URL as `git@bitbucket.org:iversoft/public-common-ios-swiftlint.git`
And choose the project where the package will be incorporated.

    ![Add package URL](img/02-packageURL.png "Add package URL")

    Note: You can choose an exact version, and use the rules set on that specific moment in time. Then check the code for issues, fix them, and move the version one step up. This will make the corrections less overwhelming, specially on large projects;
    Full documentation of the rules, their context, and version when enabled, can be found [on this confluence page](https://iversoft.atlassian.net/wiki/spaces/DEV/pages/3111256080/Swiftlint+WIP);

    Tap "Add Package" on all prompts;

3. Important: Remove the package from the Target (it should live alongside your source, not your binary). Not removing it from the target will cause the following build error:

    ![Build error](img/03-buildError.png "Build error")

    On the project properties, select the target, scroll to "Frameworks, Libraries, and Embedded Content", choose the package ("Common-Swiftlint-Package"), and click on the " - " button to delete it.

    !["Removing from target"](img/04-removeFromTarget.png "Removing from target")

4. Add a Script folder on your repository, and create the following script inside of it:

    Filename: `swiftlint-locate-and-execute.sh`
    (It will locate your swiftlint binary path (as it is different between Intel-macs and Apple Silicon-macs), as well as locate the checkout directory of the Swift Package Manager (as it depends on the set Derived Data folder - which can be different between local build and CI build))

    [Source here](Scripts/swiftlint-locate-and-execute.sh)

5. Go to the Target's Build Phases, and add a new "Run Script Phase"

    ![Add new Phase](img/05-newPhase.png "Add new Phase")

    Phase contents:

```bash

# Tests if the script exists, to avoid breaking the build if distributed outside of controlled env
ScriptFile=$SRCROOT/../Scripts/swiftlint-locate-and-execute.sh
if test -f "$ScriptFile"; then
    echo "$ScriptFile found - executing..."
    bash $ScriptFile $DERIVED_SOURCES_DIR
fi

```

Note: Just make sure the relative path matches your environment: `ScriptFile=$SRCROOT/../Scripts/`

6. Build

    If the build doesn't fail, the setup is probably completed. To double check, verify the build logs:

    ![Build logs](img/06-buildLogs.png "Build logs")
